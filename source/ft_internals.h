/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_internals.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 13:27:50 by ssantiag          #+#    #+#             */
/*   Updated: 2021/07/11 13:27:50 by ssantiag         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INTERNALS_H
# define FT_INTERNALS_H

# include <unistd.h>
# include <stdbool.h>
# include <stdarg.h>

# define BUFFER_SIZE 50
# define FL_SIGN (1)
# define FL_UPPER (2)

typedef struct s_out_params {
	int		count;
	char	buffer[BUFFER_SIZE];
}	t_out_params;

typedef struct s_params {
	int		width;
	int		precision;
	int		flags;
}	t_params;

void	out_char(char c, t_out_params *outp);
void	out_str(const char *str, t_out_params *outp, t_params params);
void	flush_buffer(t_out_params *outp);
void	c_handler(va_list va, t_out_params *outp, t_params params);
void	s_handler(va_list va, t_out_params *outp, t_params params);
void	p_handler(va_list va, t_out_params *outp, t_params params);
void	di_handler(va_list va, t_out_params *outp, t_params params);
void	u_handler(va_list va, t_out_params *outp, t_params params);
void	o_handler(va_list va, t_out_params *outp, t_params params);
void	x_handler(va_list va, t_out_params *outp, t_params params);
void	uppx_handler(va_list va, t_out_params *outp, t_params params);
void	ft_ntoa_base(long n, int base, t_out_params *outp, t_params params);
void	out_numstr(char *s, int sign, t_out_params *outp, t_params params);

#endif
