/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 14:47:40 by ssantiag          #+#    #+#             */
/*   Updated: 2021/07/11 14:47:40 by ssantiag         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_internals.h"

static int	ft_strlen(const char *s)
{
	int	len;

	len = 0;
	while (*s++ != '\0')
		len++;
	return (len);
}

void	out_char(char c, t_out_params *outp)
{
	outp->buffer[outp->count % BUFFER_SIZE] = c;
	outp->count++;
	if (outp->count != 0 && outp->count % BUFFER_SIZE == 0)
		write(STDOUT_FILENO, outp->buffer, BUFFER_SIZE);
}

void	out_str(const char *str, t_out_params *outp, t_params params)
{
	int	spaces;
	int	len;

	spaces = 0;
	len = ft_strlen(str);
	if (params.precision != -1 && params.precision < len)
		len = params.precision;
	if (params.width != -1 && params.width > len)
		spaces = params.width - len;
	while (spaces-- > 0)
		out_char(' ', outp);
	while (len-- > 0)
		out_char(*str++, outp);
}

void	flush_buffer(t_out_params *outp)
{
	write(STDOUT_FILENO, outp->buffer, outp->count % BUFFER_SIZE);
}

void	out_numstr(char *s, int sign, t_out_params *outp, t_params params)
{
	int	len;
	int	spaces;
	int	zeros;

	len = ft_strlen(s);
	zeros = 0;
	spaces = 0;
	if (params.precision != -1 && params.precision > len)
		zeros = params.precision - len;
	if (params.width != -1)
		spaces = params.width - len - zeros - sign;
	while (spaces-- > 0)
		out_char(' ', outp);
	if (sign)
		out_char('-', outp);
	while (zeros-- > 0)
		out_char('0', outp);
	while (len-- > 0)
		out_char(*s++, outp);
}
