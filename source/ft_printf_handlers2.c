/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_handlers2.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/28 17:59:29 by ssantiag          #+#    #+#             */
/*   Updated: 2021/09/28 17:59:29 by ssantiag         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_internals.h"

void	x_handler(va_list va, t_out_params *outp, t_params params)
{
	unsigned int	number;

	number = va_arg(va, unsigned int);
	params.flags = 0;
	ft_ntoa_base((unsigned long)number, 16, outp, params);
}

void	uppx_handler(va_list va, t_out_params *outp, t_params params)
{
	unsigned int	number;

	number = va_arg(va, unsigned int);
	params.flags = FL_UPPER;
	ft_ntoa_base((unsigned long)number, 16, outp, params);
}
