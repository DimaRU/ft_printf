/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/10 14:10:40 by ssantiag          #+#    #+#             */
/*   Updated: 2021/07/10 14:10:40 by ssantiag         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "ft_internals.h"

static struct s_arg_handler {
	char	type;
	void	(*handler)(va_list, t_out_params *, t_params);
} arg_handlers[] = {
	{'c', c_handler},
	{'s', s_handler},
	{'p', p_handler},
	{'d', di_handler},
	{'i', di_handler},
	{'u', u_handler},
	{'x', x_handler},
	{'X', uppx_handler},
	{'\0', NULL}
};

bool	ft_is_digit(char c)
{
	return (c >= '0' && c <= '9');
}

const char	*getnum(const char *format, int *n)
{
	*n = 0;
	while (ft_is_digit(*format) && *format != '\0')
	{
		*n = *n * 10 + *format++ - '0';
	}
	return (format);
}

const char	*parse_args(const char *format, va_list va, t_out_params *outp)
{
	struct s_arg_handler	*hadler_ptr;
	t_params				params;

	hadler_ptr = arg_handlers;
	params.width = -1;
	params.precision = -1;
	if (ft_is_digit(*format))
	{
		format = getnum(format, &params.width);
		if (*format == '.')
			format = getnum(format + 1, &params.precision);
	}
	while (hadler_ptr->handler != NULL)
	{
		if (hadler_ptr->type == *format)
		{
			hadler_ptr->handler(va, outp, params);
			return (format + 1);
		}
		hadler_ptr++;
	}
	out_char(*format, outp);
	return (format + 1);
}

int	ft_vsprintf(const char *format, va_list va)
{
	t_out_params	outp;

	outp.count = 0;
	while (*format != 0)
	{
		if (*format == '%')
			format = parse_args(format + 1, va, &outp);
		else
			out_char(*format++, &outp);
	}
	flush_buffer(&outp);
	return (outp.count);
}

int	ft_printf(const char *format, ...)
{
	int			output_count;
	va_list		va;

	va_start(va, format);
	output_count = ft_vsprintf(format, va);
	va_end(va);
	return (output_count);
}
