/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ntoabase.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/11 21:35:52 by ssantiag          #+#    #+#             */
/*   Updated: 2021/07/11 21:35:52 by ssantiag         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_internals.h"

void	ft_ntoa_base(long n, int base, t_out_params *outp, t_params params)
{
	static char	digits[] = "0123456789abcdef";
	static char	digitsu[] = "0123456789ABCDEF";
	int			sign;
	char		*buffer_ptr;
	char		buffer[66];

	sign = 1;
	if ((params.flags | FL_SIGN) && n < 0)
		sign = -1;
	buffer_ptr = buffer + sizeof(buffer);
	*(--buffer_ptr) = '\0';
	while (true)
	{
		if (params.flags & FL_UPPER)
			*(--buffer_ptr) = digitsu[sign * (n % base)];
		else
			*(--buffer_ptr) = digits[sign * (n % base)];
		n /= base;
		if (n == 0)
			break ;
	}
	if (sign < 0)
		sign = 2;
	out_numstr(buffer_ptr, sign - 1, outp, params);
}
