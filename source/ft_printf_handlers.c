/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_handlers.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/28 17:46:24 by ssantiag          #+#    #+#             */
/*   Updated: 2021/09/28 17:46:24 by ssantiag         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_internals.h"

void	c_handler(va_list va, t_out_params *outp, t_params params)
{
	char	c;

	c = va_arg(va, int);
	out_char(c, outp);
}

void	s_handler(va_list va, t_out_params *outp, t_params params)
{
	char	*str;

	str = va_arg(va, char *);
	if (str == NULL)
		out_str("(null)", outp, params);
	else
		out_str(str, outp, params);
}

void	p_handler(va_list va, t_out_params *outp, t_params params)
{
	ssize_t	number;

	out_str("0x", outp, params);
	number = va_arg(va, size_t);
	params.flags = 0;
	ft_ntoa_base(number, 16, outp, params);
}

void	di_handler(va_list va, t_out_params *outp, t_params params)
{
	int	number;

	number = va_arg(va, int);
	params.flags = FL_SIGN;
	ft_ntoa_base(number, 10, outp, params);
}

void	u_handler(va_list va, t_out_params *outp, t_params params)
{
	unsigned int	number;

	number = va_arg(va, unsigned int);
	params.flags = 0;
	ft_ntoa_base((unsigned long)number, 10, outp, params);
}
