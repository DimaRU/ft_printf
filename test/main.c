//
//  main.c
//  ft_printf
//
//  Created by Dmitriy Borovikov on 10.07.2021.
//

#include <stdio.h>
#include <limits.h>
#include "ft_printf.h"

int main(void) {
	ft_printf(">%c %c %c<\n", '1', '2', '3');
	ft_printf(">%s<\n", "Some string");
	ft_printf("int: %d %d\n", 1, -1);
	ft_printf("int: %i %i\n", 1, -1);
	ft_printf("unsigned int: %u %u\n", 1, -1);
	ft_printf("hex int: %x %x\n", 1, -1);
	ft_printf("HEX int: %X %X\n", 1, -1);
	ft_printf("ptr: %p %p\n", NULL, "123");
	ft_printf("int: %d %d\n", INT_MAX, UINT_MAX);
	ft_printf("int: %u %u\n", INT_MAX, UINT_MAX);
	ft_printf("int: %d %u\n", INT_MIN, INT_MIN);
	ft_printf("hex: %X %X\n", INT_MAX, UINT_MAX);

	printf("%6.10x!\n", 0x1234);
	ft_printf("%6.10x!\n", 0x1234);
	printf("123456789 123456789\n");
	printf("%12.10d!\n", -1234);
	ft_printf("%12.10d!\n", -1234);
	printf("%6.10s!\n", "1234");
	ft_printf("%6.10s!\n", "1234");
	printf("%10.6s!\n", "1234");
	ft_printf("%10.6s!\n", "1234");
	printf("%10.3s!\n", NULL);
	ft_printf("%10.3s!\n", NULL);
	printf("ptr: %1p\n", "123");

    return 0;
}
