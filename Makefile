# ************************************************************************** #
#                                                                            #
#                                                        :::      ::::::::   #
#   Makefile                                           :+:      :+:    :+:   #
#                                                    +:+ +:+         +:+     #
#   By: ssantiag <ssantiag@student.21-school.ru>   +#+  +:+       +#+        #
#                                                +#+#+#+#+#+   +#+           #
#   Created: 2021/07/12 09:13:45 by ssantiag          #+#    #+#             #
#   Updated: 2021/07/12 09:13:45 by ssantiag         ###   ########.fr       #
#                                                                            #
# ************************************************************************** #

SRCDIR = source
SRC =	$(SRCDIR)/ft_printf_handlers.c \
		$(SRCDIR)/ft_printf_handlers2.c \
		$(SRCDIR)/ft_ntoabase.c \
		$(SRCDIR)/ft_output.c \
		$(SRCDIR)/ft_printf.c

OBJ    = ${SRC:.c=.o}

NAME   = libftprintf.a
CC     = gcc
AR     = ar -rcs
CFLAGS = -Wall -Wextra -Werror
CFLAGS_TEST = -Wextra -Wall -Werror -g3 -fsanitize=address

all:	${NAME}

$(NAME): $(OBJ)
		$(AR) $(NAME) $(OBJ)

%.o: %.c $(SRCDIR)/ft_printf.h $(SRCDIR)/ft_internals.h
		$(CC) -c $(CFLAGS) -I $(SRCDIR) -o $@ $<

clean:
		rm -f $(OBJ)

fclean:	clean
		rm -f $(NAME)

re:		fclean all

.PHONY : clean all fclean re
